# Bachelor Thesis Impl
# Copyright (C) 2022  Fabian Kraft
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime


class DataSet:
    def __init__(self, point_id: str, start_time: datetime, end_time: datetime):
        self.point_id = point_id
        self.start_time = start_time
        self.end_time = end_time


class TestRun:
    def __init__(self, run_id: str, run_data: [DataSet]):
        self.run_id = run_id
        self.run_data = run_data
