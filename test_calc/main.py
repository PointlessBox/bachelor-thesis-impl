# Bachelor Thesis Impl
# Copyright (C) 2022  Fabian Kraft
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy
import matplotlib.pyplot as plot
import json
from dateutil import parser
import parse_data
import statistics as stat
import math
import calc
from run_types import TestRun

# 5 min x 60 seconds
TARGET_TURNAROUND_TIME = 5 * 60


def run_test_for(runs: [TestRun]):
    for run in runs:
        print(f"---- Run '{run.run_id}' ----")
        print(
            f"  PTb-3-G = Mean turnaround time (smaller is better):\t\t{calc.mean_turnaround_time(run)}")

        print(
            f"  PTb-4-G = Turnaround time adequacy (smaller is better):\t{calc.turnaround_time_adequacy(run, TARGET_TURNAROUND_TIME)}")

        print(
            f"  PTb-5-G = Mean throughput (higher is better):\t\t\t\t{calc.mean_throughput(run)}")
        print()
    print(
        f"  PTb-3-G = Mean turnaround time (ALL):\t\t\t\t\t\t{calc.mean_turnaround_time_all(runs)}")

    print(
        f"  PTb-4-G = Turnaround time adequacy (ALL):\t\t\t\t\t{calc.turnaround_time_adequacy_all(runs, TARGET_TURNAROUND_TIME)}")


def visualize_test_for(runs: [TestRun], xtick_len=None, xtick_step=None, dot_size=None):

    runs.sort(key=lambda r: len(r.run_data))
    runs.reverse()
    longest_run = runs[0]

    if dot_size is None:
        dot_size = 10

    for run in runs:
        x_values = []
        y_values = []
        for i, data_set in enumerate(run.run_data):
            time_taken = calc.adjusted_start_to_end_diff(
                data_set.start_time, data_set.end_time)
            x = i
            y = time_taken
            x_values.append(x)
            y_values.append(y)
        plot.scatter(x_values, y_values,
                     label=run.run_id, s=dot_size)

    # Plotting the Graph
    plot.xlabel("Paketindex")
    plot.ylabel("Abwicklungszeit in Sekunden")

    if xtick_len is None:
        xtick_len = len(longest_run.run_data)
    if xtick_step is None:
        xtick_step = math.ceil(xticks_len / 10)
    plot.xticks(range(0, xtick_len, xtick_step))

    plot_range = list(range(0, xtick_len + 1))
    mean_turnaround_time_all = calc.mean_turnaround_time_all(runs)
    plot.plot(plot_range,
              list(map(lambda x: mean_turnaround_time_all, plot_range)), label="∅ Abwicklungszeit", color="m")

    plot.legend(loc="best")
    plot.show()


five_point_runs = parse_data.read_and_parse_5_point_test_data()
twenty_point_runs = parse_data.read_and_parse_20_point_test_data()

# visualize_test_for(five_point_runs, xtick_len=400, xtick_step=50, dot_size=20)
# visualize_test_for(twenty_point_runs, xtick_len=1_000,
#                    xtick_step=100, dot_size=20)

print("Results of tests (Times are in seconds)")
print()

print("#### Sequential runs ####")
run_test_for(five_point_runs)
print()

print("#### Parallel runs ####")
run_test_for(twenty_point_runs)
