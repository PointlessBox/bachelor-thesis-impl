# Bachelor Thesis Impl
# Copyright (C) 2022  Fabian Kraft
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime


def parse_timestamps(lines):
    stripped = map(lambda x: x.strip(), lines)
    filtered = list(filter(lambda l: len(l) > 0, stripped))
    return filtered


pub_times = []
with open("./sys_time_pub.txt", "r") as file:
    pub_times = parse_timestamps(file.readlines())

sub_times = []
with open("./sys_time_sub.txt", "r") as file:
    sub_times = parse_timestamps(file.readlines())

diffs = []
for i, p_time in enumerate(pub_times):

    p_time = datetime.fromisoformat(pub_times[i].strip())
    s_time = datetime.fromisoformat(sub_times[i].strip())
    diffs.append((p_time - s_time).total_seconds())

average_diff = sum(diffs) / len(diffs)

print(average_diff)


# Result:
# average difference between pub sys time and sub sys time is 42.26849589655171
