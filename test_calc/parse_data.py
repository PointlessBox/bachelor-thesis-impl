# Bachelor Thesis Impl
# Copyright (C) 2022  Fabian Kraft
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy
import matplotlib.pyplot as plot
import json
from dateutil import parser
import os
import run_types


def relative_path_from(path: str) -> str:
    return os.path.join(".", path)


def get_sequential_run_data_as_json():
    """
    Reads all data files from ./in/sequential and returns their contents as a json array.
    """
    result = []
    path = relative_path_from(os.path.join("in", "sequential"))
    for f_name in os.listdir(path):
        joined_path = os.path.join(path, f_name)
        if os.path.isfile(joined_path):
            run_id = f_name.removeprefix("test_data_")
            run_id = run_id.removesuffix(".json")
            with open(joined_path, "r") as file:
                data_sets = json.loads(file.read())
                result.append({
                    "runId": run_id,
                    "dataSets": data_sets
                })
    return result


def get_parallel_run_data_as_json():
    """
    Reads all data files from ./in/parallel and returns their contents as a json array.
    """
    result = []
    path = relative_path_from(os.path.join("in", "parallel"))
    for d_name in os.listdir(path):
        run_dir = os.path.join(path, d_name)
        run_id = d_name.removeprefix("multi_")
        data_sets = []
        for f_name in os.listdir(run_dir):
            point_file = os.path.join(run_dir, f_name)
            if os.path.isfile(point_file):
                with open(point_file, "r") as file:
                    data_sets += json.loads(file.read())
        result.append({
            "runId": run_id,
            "dataSets": data_sets
        })
    return result


def data_set_from_json(json_data_set) -> run_types.DataSet:
    point_id = json_data_set["pointId"]
    start_time = parser.parse(json_data_set["start"])
    end_time = parser.parse(json_data_set["end"])
    return run_types.DataSet(point_id, start_time, end_time)


def read_and_parse_5_point_test_data() -> [run_types.TestRun]:
    """
    Reads the run data in ./in/sequential and returns it as [TestRunFor5Points]
    """
    json_run_data = get_sequential_run_data_as_json()

    five_point_run_data: [run_types.TestRun] = []

    for test_run in json_run_data:
        run_id = test_run["runId"]
        run_data: [run_types.DataSet] = []

        for json_data_set in test_run["dataSets"]:
            run_data.append(data_set_from_json(json_data_set))

        five_point_run_data.append(
            run_types.TestRun(run_id, run_data))

    return five_point_run_data


def read_and_parse_20_point_test_data() -> [run_types.TestRun]:
    """
    Reads the run data in ./in/parallel and returns it as [TestRunFor5Points]
    """
    json_run_data = get_parallel_run_data_as_json()

    twenty_point_run_data: [run_types.TestRun] = []

    for test_run in json_run_data:
        run_id = test_run["runId"]
        run_data: [run_types.DataSet] = []

        for json_data_set in test_run["dataSets"]:
            run_data.append(data_set_from_json(json_data_set))

        twenty_point_run_data.append(
            run_types.TestRun(run_id, run_data))

    return twenty_point_run_data
