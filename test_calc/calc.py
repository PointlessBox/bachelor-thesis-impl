# Bachelor Thesis Impl
# Copyright (C) 2022  Fabian Kraft
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy
import matplotlib.pyplot as plot
import json
from dateutil import parser
import parse_data
import statistics as stat
import math
from run_types import TestRun
from datetime import datetime

# diff = sys_time_pub - sys_time_sub  (in seconds)
AVERAGE_TIME_DIFF = 42.26849589655171

NO_OF_DIGITS = 4


def adjusted_start_to_end_diff(start_time: datetime, end_time: datetime) -> float:
    """
    Calculates the difference end_time - start_time and adds system time difference
    """
    return (end_time - start_time).total_seconds() + AVERAGE_TIME_DIFF


def mean_turnaround_time(test_run: TestRun) -> float:
    """
    Calculates the mean turnaround time of a number of jobs according to PTb-3-G in ISO/IEC 25023
    """

    sum_i_to_n = 0
    n = len(test_run.run_data)

    for data_set in test_run.run_data:
        sum_i_to_n += adjusted_start_to_end_diff(
            data_set.start_time, data_set.end_time)

    return round(sum_i_to_n / n, NO_OF_DIGITS)


def mean_turnaround_time_all(test_runs: [TestRun]) -> float:
    """
    Calculates the mean turnaround time of a number of jobs according to PTb-3-G in ISO/IEC 25023
    """

    all_test_runs = []

    for run in test_runs:
        all_test_runs += run.run_data

    sum_i_to_n = 0
    n = len(all_test_runs)

    for data_set in all_test_runs:
        sum_i_to_n += adjusted_start_to_end_diff(
            data_set.start_time, data_set.end_time)

    return round(sum_i_to_n / n, NO_OF_DIGITS)


def turnaround_time_adequacy(test_run: TestRun, target_turnaround_time: float) -> float:
    """
    Calculates the turnaround time adequacy of a number of jobs according to PTb-3-G in ISO/IEC 25023
    """
    return round(mean_turnaround_time(test_run) / target_turnaround_time, NO_OF_DIGITS)


def turnaround_time_adequacy_all(test_runs: [TestRun], target_turnaround_time: float) -> float:
    """
    Calculates the turnaround time adequacy of a number of jobs according to PTb-3-G in ISO/IEC 25023
    """
    return round(mean_turnaround_time_all(test_runs) / target_turnaround_time, NO_OF_DIGITS)


def mean_throughput(test_run: TestRun) -> float:
    """
    Calculates the mean throughput of a number of jobs according to PTb-3-G in ISO/IEC 25023
    """

    run_data = test_run.run_data
    run_data.sort(key=lambda s: s.start_time)

    observation_start = run_data[0].start_time
    observation_end = run_data[len(run_data) - 1].start_time
    last_second_of_observation = math.ceil(
        (observation_end - observation_start).total_seconds()) + 1
    observations = range(1, last_second_of_observation + 1)

    # Initialize each observation with a job count of 0
    job_count_per_observation = dict(map(lambda i: [i, 0], observations))

    for ith_obs in observations:
        for job in run_data:
            # The second from observation_start where the job was started
            start_second = (job.start_time - observation_start).total_seconds()
            # Diff between the second of observation and the start time of the job
            time_diff_to_start_time = ith_obs - start_second
            # If start time of job is in same second as the i-th observation
            if time_diff_to_start_time >= 0.0 and time_diff_to_start_time < 1:
                # Then add a count to the i-th observation
                job_count_per_observation[ith_obs] += 1

    sum_i_to_n = 0
    n = len(observations)

    for ith_obs in job_count_per_observation:
        sum_i_to_n += job_count_per_observation[ith_obs] / ith_obs

    return round(sum_i_to_n / n, NO_OF_DIGITS)
