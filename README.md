# Bachelor Thesis Impl

Implementation of a publish- / subscribe-architecture using the iota-channels-protocol, for my bachelor-thesis titled "Investigating the tangle-technologie for development of an application for secure data transmission of machine measurement data".

# Do a test run

## Setup

I used the PNPM instead of NPM, but they probably both work. If NPM does not work then download it [here](https://pnpm.io/). Also make sure you have the [Rust-Toolchain](https://www.rust-lang.org/tools/install) installed.

Generally the publisher and subscriber redirect both their stdout and stderr into a `log.txt` inside their folders. You can watch them by using

```bash
watch -n 0.2 "tail -n 25 log.txt"
```

### Setup access-control-server

Change directory to `./access_control` and run the following to fix an issue with `sequelize`

```bash
pnpm install; pnpm rebuild
```

Now create the SQLite database-file by running

```bash
mkdir db && touch db/database.db
```

Now start the access-control-server by running

```bash
pnpm start:dev
```

### Setup publisher and subscriber

Inside `./publisher` and `./subscriber` run

```bash
cargo build --release
```

There are also some bash-scripts which are used to initialize and start the test-runs. Make them executable by running

```bash
chmod +x <BASH-SCRIPT-FILE>
```

for each `<BASH-SCRIPT-FILE>`

## Initializing the database

Before you start, make sure the access-control-server is already running on your machine.

Inside `./publisher` run the `init_n_points.sh` file like so:

```bash
./init_n_points.sh <NUMBER_OF_POINTS> <NODE_URL> <SERVER_URL>
```

where `<NUMBER_OF_POINTS>` is the number of parallel running measuring points you want to simulate; `<NODE_URL>` is the IOTA-Node you want to connect to; `<SERVER_URL>` is the url to your access-control-server.

Example:

```bash
./init_n_points.sh 5 https://chrysalis-nodes.iota.org http://localhost:3000
```

Wait until the running access-control-server process does not output anything anymore.

## Starting the subscriber

There is no guarantee that your subscribers will shut down, because they will await a "END"-Message for the point they are watching. To explicitly send a "END"-Message, use the CLI-Tool for the publisher. Inside `./publisher` use

```bash
./target/release/publisher --help
```
to get further information.

---

After all points have been initialized in the database, you can start the subscribers.

Inside `./subscriber` run

```bash
./start_sub_n_points.sh <NUMBER_OF_POINTS> <NODE_URL> <SERVER_URL>
```

The parameters should match the ones your used with `publisher/init_n_points.sh`.

Wait until the is no more output on your console.

Example:

```bash
./start_sub_n_points.sh 5 https://chrysalis-nodes.iota.org http://localhost:3000
```

## Start the publishers

After you started the subscribers, you can start the publishers.

Inside `./publisher` run

```bash
./start_pub_n_points.sh <NUMBER_OF_POINTS> <NUMBER_OF_MSGS> <NODE_URL> <SERVER_URL>
```

The parameters `<NUMBER_OF_POINTS>`, `<NODE_URL>` and `<SERVER_URL>` should match the ones in the other calls. `<NUMBER_OF_MSGS>` is the number of messages each publisher (point) should send.

Example:

```bash
./start_pub_n_points.sh 5 10 https://chrysalis-nodes.iota.org http://localhost:3000
```
