// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use chrono::Utc;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TestData {
    pub point_id: String,
    pub count: usize,
    /// chrono::Utc::now().to_rfc3339()
    pub start: String,
    /// chrono::Utc::now().to_rfc3339()
    pub end: Option<String>,
}

impl TestData {
    pub fn new(point_id: &String, count: usize) -> Self {
        TestData {
            point_id: point_id.clone(),
            count,
            start: Utc::now().to_rfc3339(),
            end: None,
        }
    }
}
