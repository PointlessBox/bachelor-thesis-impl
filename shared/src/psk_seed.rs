// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::Result;
use iota_streams::app_channels::api::{psk_from_seed, pskid_from_psk, Psk, PskId};

const SEED_LENGTH: usize = 32;

pub struct PskSeed {
    bytes: [u8; SEED_LENGTH],
}

impl PskSeed {
    /// Returns the bytes of the seed as an array with length of 32
    pub fn as_bytes(&self) -> &[u8; SEED_LENGTH] {
        &self.bytes
    }
}

impl TryFrom<Vec<u8>> for PskSeed {
    type Error = anyhow::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        if value.len() == SEED_LENGTH {
            let mut seed_bytes: [u8; SEED_LENGTH] = [0; SEED_LENGTH];
            value.into_iter().enumerate().for_each(|(index, byte)| {
                if index <= seed_bytes.len() {
                    seed_bytes[index] = byte
                }
            });
            Ok(Self { bytes: seed_bytes })
        } else {
            Err(Self::Error::new(std::fmt::Error))
        }
    }
}

impl ToString for &PskSeed {
    /// Returns the bytes of the seed encoded as base64
    fn to_string(&self) -> String {
        base64::encode(self.bytes)
    }
}

/// Generates a pre-shared-key (PSK) from the given PskSeed
/// # Returns
/// A tuple with `(psk, psk_id)`
pub fn generate_psk(seed: &PskSeed) -> (Psk, PskId) {
    // let key_seed = rand::thread_rng().gen::<[u8; 32]>();
    let key_seed = seed.as_bytes();
    // Generate a pre-shared-key from that seed
    let psk = psk_from_seed(key_seed);
    // Get the id of the psk
    let psk_id = pskid_from_psk(&psk);

    // Return both as a tuple
    return (psk, psk_id);
}
