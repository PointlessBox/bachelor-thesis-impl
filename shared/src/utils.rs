// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub mod env {
    use std::{
        collections::HashMap,
        env::{self, Vars},
    };

    pub const ENV_NODE_URL: &str = "NODE_URL";
    pub const ENV_SERVER_URL: &str = "SERVER_URL";
    pub const ENV_POINT_ID: &str = "POINT_ID";

    /// Checks if the given Vars contain the ones needed for this program,
    /// and returns them if they were found
    ///
    /// # Arguments
    /// - `vars` - std::env::Vars to check
    ///
    /// # Returns
    /// - `Result<HashMap<String, String>>` with the needed environment variables
    pub fn check_env_vars(vars: Vars) -> Result<(), String> {
        let vars_map = vars.collect::<HashMap<String, String>>();
        let has_node_url = vars_map.contains_key(ENV_NODE_URL);
        let has_server_url = vars_map.contains_key(ENV_SERVER_URL);
        let has_point_id = vars_map.contains_key(ENV_POINT_ID);

        if !has_node_url {
            return Err("'NODE_URL' does not exist in environment".into());
        }
        if !has_server_url {
            return Err("'SERVER_URL' does not exist in environment".into());
        }
        if !has_point_id {
            return Err("'POINT_ID' does not exist in environment".into());
        }

        Ok(())
    }

    /// Returns the unwrapped SERVER_URL environment variable. This can only be called safely after calling 'check_env_vars()' before.
    pub fn get_unwrapped_server_url() -> String {
        // This will only be safe if the program checked the env vars by calling 'check_env_vars()' before
        return env::var(ENV_SERVER_URL).unwrap();
    }
}

pub mod seed {
    use rand::Rng;
    /// Generates a random seed with 32 byte length and encodes it into a base64 string
    pub fn generate_with_32_bytes() -> String {
        let random_bytes = rand::thread_rng().gen::<[u8; 32]>();
        base64::encode(&random_bytes)
    }
}

pub fn print_success() {
    println!("✓");
}

pub fn print_fail() {
    println!("⨯");
}
