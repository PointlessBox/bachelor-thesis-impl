// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::{Error, Result};
use std::collections::HashMap;
use std::fs::{self, File};
use std::io::prelude::*;

pub trait Store {
    fn store<V: ToString>(key: String, value: V) -> Result<()>;
    fn read(key: String) -> Result<String>;
}

trait InnerStore {
    fn store<V: ToString>(key: String, value: V, file_path: &str) -> Result<()>;
    fn read(key: String, file_path: &str) -> Result<String>;
}

const JSON_STORE_DIR: &str = "../store";
const JSON_STORE_LOCATION: &str = "../store/json_store.json";
const JSON_SEED_STORE_LOCATION: &str = "../store/json_seed_store.json";

pub struct KeyValueStore {}

impl Store for KeyValueStore {
    fn store<V: ToString>(key: String, value: V) -> Result<()> {
        JsonFileStore::store(key, value, JSON_STORE_LOCATION)
    }
    fn read(key: String) -> Result<String> {
        JsonFileStore::read(key, JSON_STORE_LOCATION)
    }
}

pub struct AuthorSeedStore {}

impl AuthorSeedStore {
    /// Reads key-value-pairs of point ids and author seeds as a HashMap
    pub fn read_all_seeds() -> Result<HashMap<String, String>> {
        JsonFileStore::read_all(JSON_SEED_STORE_LOCATION)
    }
}

impl Store for AuthorSeedStore {
    /// Stores an author seed for a point id
    fn store<V: ToString>(point_id: String, author_seed: V) -> Result<()> {
        JsonFileStore::store(point_id, author_seed, JSON_SEED_STORE_LOCATION)
    }
    fn read(point_id: String) -> Result<String> {
        JsonFileStore::read(point_id, JSON_SEED_STORE_LOCATION)
    }
}

struct JsonFileStore {}

impl JsonFileStore {
    fn read_all(file_path: &str) -> Result<HashMap<String, String>> {
        if let Ok(_) = fs::create_dir_all(file_path) {};
        let mut store_content = String::new();

        if let Ok(mut file) = File::open(file_path) {
            file.read_to_string(&mut store_content)?;
        }

        let json: HashMap<String, String> = serde_json::from_str(store_content.as_str())?;
        Ok(json)
    }
}

impl InnerStore for JsonFileStore {
    fn store<V: ToString>(key: String, value: V, file_path: &str) -> Result<()> {
        if let Ok(_) = fs::create_dir_all(JSON_STORE_DIR) {};
        let mut store_content = String::new();

        if let Ok(mut file) = File::open(file_path) {
            file.read_to_string(&mut store_content)?;
        }

        // Add empty JSON object if store is empty
        if store_content.len() < 1 {
            eprintln!("File was empty");
            store_content += "{}";
        }

        let store_json_value: serde_json::Value = serde_json::from_str(store_content.as_str())?;
        let mut store_object = if let Some(obj) = store_json_value.as_object() {
            obj.to_owned()
        } else {
            eprintln!("Could not parse store");
            return Err(Error::new(std::fmt::Error));
        };
        store_object.insert(key, serde_json::Value::String(value.to_string()));

        let mut store_file = File::create(file_path)?;

        if let Ok(json_str) = serde_json::to_string(&store_object) {
            store_file.write_all(json_str.as_bytes())?;
        } else {
            eprintln!("Could not save store");
            return Err(Error::new(std::fmt::Error));
        }

        Ok(())
    }

    fn read(key: String, file_path: &str) -> Result<String> {
        let mut store_file = File::open(file_path)?;

        let mut store_content = String::new();
        store_file.read_to_string(&mut store_content)?;

        let store_json: serde_json::Value = serde_json::from_str(store_content.as_str())?;
        let value = if let Some(obj) = store_json.as_object() {
            if let Some(val_enum) = obj.get(&key) {
                if let serde_json::Value::String(value) = val_enum {
                    value
                } else {
                    eprintln!("Could not get {:?}", val_enum);
                    return Err(Error::new(std::fmt::Error));
                }
            } else {
                eprintln!("Could not get {:?}", obj.get(&key));
                return Err(Error::new(std::fmt::Error));
            }
        } else {
            eprintln!("Could not read {}", key);
            return Err(Error::new(std::fmt::Error));
        }
        .to_owned();

        Ok(value)
    }
}
