// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use chrono::Utc;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TimedMeasurement {
    pub timestamp: String,
    pub measure_point_id: String,
    pub measurement: f64,
}

impl TimedMeasurement {
    pub fn new(measure_point_id: &String, measurement: f64) -> Self {
        TimedMeasurement {
            timestamp: Utc::now().to_rfc3339(),
            measure_point_id: measure_point_id.clone(),
            measurement,
        }
    }
}
