// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::fs::{self, File};
use std::io::Write;

use crate::subscriber_repository::{MultiPointRepo, RemoteMultiPointRepo};
use anyhow::Result;
use chrono::Utc;
use iota_streams::app::transport::tangle::client::Client;
use iota_streams::app_channels::api::tangle::futures::TryStreamExt;
use iota_streams::app_channels::api::tangle::Subscriber;
use iota_streams::app_channels::Bytes;
use shared::model::Point;
use shared::test_data::TestData;
use shared::utils;

fn generate_subscriber(node_url: &String) -> Subscriber<Client> {
    let client = Client::new_from_url(node_url);
    let sub_seed = utils::seed::generate_with_32_bytes();
    // Subscriber implementation does not need to specify a channel type, it will be
    // parsed from the announcement message
    let subscriber = Subscriber::new(sub_seed.as_str(), client);
    subscriber
}

type PointSubscriber = (Point, Subscriber<Client>);

pub async fn run_multi_point(node_url: &String, server: &String) -> Result<()> {
    println!("run_multi_point()");

    let all_points = RemoteMultiPointRepo::get_all_points(server).await?;

    let mut subscribers: Vec<PointSubscriber> = vec![];
    for point in all_points.into_iter() {
        println!(
            "Generating subscriber for point '{}'",
            point.measure_point_id
        );
        let mut subscriber = generate_subscriber(node_url);
        let point_id = &point.measure_point_id;

        print!("    Loading announcement link ... ");
        let ann_link = RemoteMultiPointRepo::try_load_announcement_link(point_id, server).await?;
        subscriber.receive_announcement(&ann_link).await?;
        shared::utils::print_success();

        print!("    Loading psk ... ");
        let psk_seed = RemoteMultiPointRepo::try_load_psk_seed(point_id, server).await?;
        let (psk, psk_id) = shared::psk_seed::generate_psk(&psk_seed);
        subscriber.store_psk(psk_id, psk)?;
        shared::utils::print_success();

        print!("    Loading keyload link ... ");
        let keyload_link = RemoteMultiPointRepo::try_load_keyload_link(point_id, server).await?;
        subscriber.receive_keyload(&keyload_link).await?;
        shared::utils::print_success();

        print!("    Syncing state ... ");
        subscriber.sync_state().await?;
        shared::utils::print_success();

        subscribers.push((point, subscriber));
    }

    match receive_test_data(subscribers).await {
        Ok(_) => println!("Receiving test data completed ✓"),
        Err(err) => {
            println!("Receiving test data failed ⨯");
            eprintln!("{}", err);
        }
    }
    Ok(())
}

fn test_data_from_json(json_string: &str) -> Result<TestData> {
    let test_data: TestData = serde_json::from_str(json_string)?;
    return Ok(test_data);
}

async fn receive_test_data(mut subscribers: Vec<PointSubscriber>) -> Result<()> {
    let mut received_data: Vec<TestData> = vec![];
    let mut end_msg_count = 0;
    loop {
        for (_point, sub) in subscribers.iter_mut() {
            let mut messages = sub.messages();
            if let Ok(next) = messages.try_next().await {
                if let Some(msg) = next {
                    let masked_pl = msg
                        .body
                        .masked_payload()
                        .and_then(Bytes::as_str)
                        .unwrap_or("None");

                    println!("{masked_pl}");

                    if masked_pl == "END" {
                        end_msg_count += 1;
                    } else {
                        let received_timestamp = Utc::now().to_rfc3339();

                        let mut test_data = test_data_from_json(masked_pl)?;
                        test_data.end = Some(received_timestamp);
                        received_data.push(test_data);
                    }
                }
            }
        }
        // Ends if all publishers sent their 'END' message
        if end_msg_count == subscribers.len() {
            break;
        }
    }
    save_test_data(&received_data)
}

const OUT_DIR: &str = "./out";
const TEST_DATA_FILE: &str = "./out/test_data.json";

fn save_test_data(test_data: &Vec<TestData>) -> Result<()> {
    if let Ok(_) = fs::create_dir_all(OUT_DIR) {};
    let mut store_file = File::create(TEST_DATA_FILE)?;

    if let Ok(json_str) = serde_json::to_string(test_data) {
        store_file.write_all(json_str.as_bytes())?;
    };

    Ok(())
}
