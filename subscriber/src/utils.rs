// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::Result;
use chrono::Utc;
use iota_streams::app::transport::tangle::client::Client;
use iota_streams::app_channels::api::tangle::futures::TryStreamExt;
use iota_streams::app_channels::api::tangle::Subscriber;
use iota_streams::app_channels::Bytes;
use shared::model::Point;
use shared::test_data::TestData;
use std::fs::{self, File};
use std::io::Write;

pub type PointSubscriber = (Point, Subscriber<Client>);

fn test_data_from_json(json_string: &str) -> Result<TestData> {
    let test_data: TestData = serde_json::from_str(json_string)?;
    return Ok(test_data);
}

pub async fn receive_test_data(mut subscribers: Vec<PointSubscriber>) -> Result<()> {
    let mut received_data: Vec<TestData> = vec![];
    let mut end_msg_count = 0;
    loop {
        for (point, sub) in subscribers.iter_mut() {
            // println!("COUNT: {}", end_msg_count);
            // sub.sync_state().await?;
            let mut messages = sub.messages();
            if let Ok(next) = messages.try_next().await {
                if let Some(msg) = next {
                    let masked_pl = msg
                        .body
                        .masked_payload()
                        .and_then(Bytes::as_str)
                        .unwrap_or("None");

                    println!("{masked_pl}");

                    if masked_pl == "END" {
                        end_msg_count += 1;
                    } else {
                        let received_timestamp = Utc::now().to_rfc3339();

                        let mut test_data = test_data_from_json(masked_pl)?;
                        test_data.end = Some(received_timestamp);
                        received_data.push(test_data);
                    }
                    save_test_data(&received_data, &point.measure_point_id)?;
                }
            }
        }
        // Ends if all publishers sent their 'END' message
        if end_msg_count == subscribers.len() {
            break;
        }
    }
    Ok(())
}

const OUT_DIR: &str = "./out";

fn save_test_data(test_data: &Vec<TestData>, point_id: &String) -> Result<()> {
    if let Ok(_) = fs::create_dir_all(OUT_DIR) {};
    let out_file = format!("./out/test_data-{}.json", point_id.as_str());
    let mut store_file = File::create(out_file)?;

    if let Ok(json_str) = serde_json::to_string(test_data) {
        store_file.write_all(json_str.as_bytes())?;
    };

    Ok(())
}
