// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use crate::subscriber_repository::{MultiPointRepo, RemoteMultiPointRepo};
use anyhow::Result;
use iota_streams::app::transport::tangle::client::Client;
use iota_streams::app_channels::api::tangle::Subscriber;
use shared::model::Point;
use shared::utils;

fn generate_subscriber(node_url: &String) -> Subscriber<Client> {
    let client = Client::new_from_url(node_url);
    let sub_seed = utils::seed::generate_with_32_bytes();
    // Subscriber implementation does not need to specify a channel type, it will be
    // parsed from the announcement message
    let subscriber = Subscriber::new(sub_seed.as_str(), client);
    subscriber
}

type PointSubscriber = (Point, Subscriber<Client>);

pub async fn subscribe(point_id: &String, node_url: &String, server: &String) -> Result<()> {
    println!("single_point::subscribe()");

    let point = RemoteMultiPointRepo::get_point(point_id, server).await?;

    let mut subscribers: Vec<PointSubscriber> = vec![];
    println!(
        "Generating subscriber for point '{}'",
        point.measure_point_id
    );
    let mut subscriber = generate_subscriber(node_url);
    let point_id = &point.measure_point_id;

    print!("    Loading announcement link ... ");
    let ann_link = RemoteMultiPointRepo::try_load_announcement_link(point_id, server).await?;
    subscriber.receive_announcement(&ann_link).await?;
    shared::utils::print_success();

    print!("    Loading psk ... ");
    let psk_seed = RemoteMultiPointRepo::try_load_psk_seed(point_id, server).await?;
    let (psk, psk_id) = shared::psk_seed::generate_psk(&psk_seed);
    subscriber.store_psk(psk_id, psk)?;
    shared::utils::print_success();

    print!("    Loading keyload link ... ");
    let keyload_link = RemoteMultiPointRepo::try_load_keyload_link(point_id, server).await?;
    subscriber.receive_keyload(&keyload_link).await?;
    shared::utils::print_success();

    print!("    Syncing state ... ");
    subscriber.sync_state().await?;
    shared::utils::print_success();

    subscribers.push((point, subscriber));

    match crate::utils::receive_test_data(subscribers).await {
        Ok(_) => println!("Receiving test data completed ✓"),
        Err(err) => {
            println!("Receiving test data failed ⨯");
            eprintln!("{}", err);
        }
    }

    Ok(())
}
