// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use clap::{Parser, Subcommand};

#[derive(Subcommand)]
pub enum Subcommands {
    Subscribe {
        /// The id of the point to subscribe to
        point_id: Option<String>,
        /// Node URL of the IOTA-Node to use
        #[clap(short, long)]
        node_url: String,
        /// Access control server URL
        #[clap(short, long)]
        server: String,
        /// Whether all available publishers should be subscribed
        #[clap(short, long)]
        all: bool,
    },
}

/// Simple program to greet a person
#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Subcommands,
}
