// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::{anyhow, Result};
use async_trait::async_trait;
use iota_streams::app::transport::tangle::TangleAddress;
use shared::{model::Point, psk_seed::PskSeed};
use std::str::FromStr;

#[async_trait]
pub trait MultiPointRepo {
    async fn get_point(point_id: &String, server_url: &String) -> Result<Point>;
    async fn get_all_points(server_url: &String) -> Result<Vec<Point>>;
    async fn try_load_announcement_link(
        point_id: &String,
        server_url: &String,
    ) -> Result<TangleAddress>;
    async fn try_load_psk_seed(point_id: &String, server_url: &String) -> Result<PskSeed>;
    async fn try_load_keyload_link(point_id: &String, server_url: &String)
        -> Result<TangleAddress>;
}

pub struct RemoteMultiPointRepo {}

#[async_trait]
impl MultiPointRepo for RemoteMultiPointRepo {
    async fn get_point(point_id: &String, server_url: &String) -> Result<Point> {
        // let base_url = utils::env::get_unwrapped_server_url();
        let url = format!("{}/point/{}", server_url, point_id);
        println!("requesting {url}");
        let point = reqwest::get(url).await?.json::<Point>().await?;
        Ok(point)
    }

    async fn get_all_points(server_url: &String) -> Result<Vec<Point>> {
        // let base_url = utils::env::get_unwrapped_server_url();
        let url = format!("{}/point", server_url);
        let points = reqwest::get(url).await?.json::<Vec<Point>>().await?;
        Ok(points)
    }

    async fn try_load_announcement_link(
        point_id: &String,
        server_url: &String,
    ) -> Result<TangleAddress> {
        let point = Self::get_point(point_id, server_url).await?;
        let ann_link = if let Some(link) = point.announcement_link {
            link
        } else {
            return Err(anyhow!("point.announcement_link was None"));
        };
        let ann_link = TangleAddress::from_str(ann_link.as_str())?;

        Ok(ann_link)
    }

    async fn try_load_psk_seed(point_id: &String, server_url: &String) -> Result<PskSeed> {
        let point = Self::get_point(point_id, server_url).await?;
        let psk_seed = if let Some(seed) = point.pre_shared_key_seed {
            seed
        } else {
            return Err(anyhow!("point.pre_shared_key_seed was None"));
        };
        let seed_bytes = base64::decode(&psk_seed)?;
        let psk_seed = PskSeed::try_from(seed_bytes)?;

        return Ok(psk_seed);
    }

    async fn try_load_keyload_link(
        point_id: &String,
        server_url: &String,
    ) -> Result<TangleAddress> {
        let point = Self::get_point(point_id, server_url).await?;
        let keyload_link = if let Some(link) = point.keyload_link {
            link
        } else {
            return Err(anyhow!("point.keyload_link was None"));
        };
        let keyload_link = TangleAddress::from_str(keyload_link.as_str())?;

        Ok(keyload_link)
    }
}
