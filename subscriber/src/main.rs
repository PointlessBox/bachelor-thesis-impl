// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::Result;
use clap::Parser;
use run_multi_point::run_multi_point;

mod cli;
mod run_multi_point;
mod single_point;
mod subscriber_repository;
mod utils;

#[allow(dead_code)]
const PWD: &str = "SUPER SAFE!!11!";

#[tokio::main]
async fn main() -> Result<()> {
    let cli = cli::Cli::parse();
    let cli::Subcommands::Subscribe {
        point_id,
        node_url,
        server,
        all,
    } = cli.command;

    if all {
        run_multi_point(&node_url, &server).await?;
    } else {
        if let Some(point_id) = point_id {
            single_point::subscribe(&point_id, &node_url, &server).await?;
        }
    }

    Ok(())
}
