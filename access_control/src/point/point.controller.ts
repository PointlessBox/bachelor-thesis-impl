// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { logWithResult } from 'src/utils';
import { Point } from './point.model';
import { PointService } from './point.service';

@Controller('point')
export class PointController {
  constructor(private pointService: PointService) {}

  @Get()
  async getAll(): Promise<Point[]> {
    console.log('getAll called');
    return logWithResult(await this.pointService.findAll());
  }

  @Get(':id')
  async getOne(@Param('id') id: string): Promise<Point> {
    console.log('getOne called');
    return logWithResult(await this.pointService.findOne(id));
  }

  @Post()
  async create(@Body() createPoint: Point): Promise<Point> {
    console.log('create called');
    console.log(createPoint);
    return logWithResult(await this.pointService.create(createPoint));
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<void> {
    console.log('delete called');
    return logWithResult(await this.pointService.remove(id));
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updatePoint: Point,
  ): Promise<Point> {
    console.log('update called');
    return logWithResult(await this.pointService.update(id, updatePoint));
  }
}
