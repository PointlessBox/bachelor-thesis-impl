// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { PointDto } from './point.entity';
import { Point } from './point.model';

@Injectable()
export class PointService {
  constructor(
    @InjectModel(PointDto)
    private pointModel: typeof PointDto,
  ) {}

  async findAll(): Promise<Point[]> {
    return (await this.pointModel.findAll()).map((pDto) =>
      Point.fromPointDto(pDto),
    );
  }

  async findOne(measurePointId: string): Promise<Point> {
    const pointDto = await this.pointModel.findOne({
      where: {
        measurePointId,
      },
    });
    return pointDto != null ? Point.fromPointDto(pointDto) : null;
  }

  async create(point: Point): Promise<Point> {
    const createdPoint = await this.pointModel.create({ ...point });
    return Point.fromPointDto(createdPoint);
  }

  async update(measurePointId: string, point: Point): Promise<Point> {
    const [updatedPoint] = await this.pointModel.upsert({
      ...point,
      // inserting the actual id afterwards ignores the id given by spreading the point object
      measurePointId,
    });
    return Point.fromPointDto(updatedPoint);
  }

  async remove(measurePointId: string): Promise<void> {
    const point = await await this.pointModel.findOne({
      where: {
        measurePointId,
      },
    });
    await point.destroy();
  }
}
