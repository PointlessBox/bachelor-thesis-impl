#!/bin/bash


# Bachelor Thesis Impl
# Copyright (C) 2022  Fabian Kraft
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

point_count=$1
echo "--- DEPRECATED ---"
echo $point_count

for i in $(seq 0 ($point_count-1));
do
    POINT_ID=MP-$i ./target/release/publisher >> log.txt &
    sleep 0.15
done
