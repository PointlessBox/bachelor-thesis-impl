// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use clap::{Parser, Subcommand};

#[derive(Subcommand)]
pub enum Subcommands {
    /// Sends an "END" message for points
    End {
        /// The point to send an "END" message for
        point_id: Option<String>,
        /// Send an "END" message for all points
        #[clap(short, long)]
        all: bool,
    },
    /// Start publishing for points
    Publish {
        /// The id of the point to publish for
        point_id: Option<String>,
        /// Number of messages to send
        msg_count: Option<usize>,
        /// Whether all this instance should publish for all available points
        #[clap(short, long)]
        all: bool,
    },
    /// Generate and initialize points
    Init {
        /// The point to generate initial data for
        point_id: String,
    },
}

/// Simple program to greet a person
#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Subcommands,

    /// Node URL of the IOTA-Node to use
    #[clap(short, long)]
    pub node_url: String,
    /// Access control server URL
    #[clap(short, long)]
    pub server: String,
}
