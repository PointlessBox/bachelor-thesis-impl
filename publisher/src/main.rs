// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::Result;
use clap::Parser;
use iota_streams::app::transport::tangle::TangleAddress;
use point_author::PointAuthor;
use publisher_repository::{self as repo};

pub mod as_payload;
pub mod cli;
pub mod end;
pub mod init_test_data;
pub mod point_author;
pub mod publish;
pub mod publisher_repository;
pub mod utils;

type PrevMsgLink = TangleAddress;

#[tokio::main]
async fn main() -> Result<()> {
    let cli = cli::Cli::parse();
    let node_url = cli.node_url;
    let server = cli.server;
    match cli.command {
        cli::Subcommands::End { all, point_id } => {
            on_end_cmd(all, point_id, node_url, server).await?
        }
        cli::Subcommands::Publish {
            point_id,
            msg_count,
            all,
        } => on_pub_cmd(point_id, msg_count, all, node_url, server).await?,
        cli::Subcommands::Init { point_id } => on_init_cmd(point_id, node_url, server).await?,
    };

    Ok(())
}

async fn on_end_cmd(
    all: bool,
    point_id: Option<String>,
    node_url: String,
    server: String,
) -> Result<()> {
    if all {
        let all_points = utils::recover_all_authors(&node_url, &server).await?;
        end::multiple(all_points).await?;
    } else {
        let point_id = point_id.expect("No point id was given");
        let (last_msg_link, author) =
            utils::recover_author_of_point(&point_id, &node_url, &server).await?;
        shared::utils::print_success(); // Shows if recovery was successful
        let point_author = PointAuthor {
            point_id,
            author,
            last_msg_link,
        };
        end::multiple(vec![point_author]).await?;
    }
    Ok(())
}

async fn on_init_cmd(point_id: String, node_url: String, server: String) -> Result<()> {
    init_test_data::generate_init_data_for(&point_id, &node_url, &server).await?;
    Ok(())
}

async fn on_pub_cmd(
    point_id: Option<String>,
    msg_count: Option<usize>,
    all: bool,
    node_url: String,
    server: String,
) -> Result<()> {
    if all {
        todo!("Implement publish --all")
    }
    publish::single_point(
        point_id.expect("No point id was given"),
        msg_count.expect("No msg count was given"),
        node_url,
        server,
    )
    .await
}
