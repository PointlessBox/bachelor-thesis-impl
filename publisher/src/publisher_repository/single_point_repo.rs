// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::Result;
use async_trait::async_trait;
use iota_streams::app::transport::tangle::TangleAddress;
use shared::{
    psk_seed::PskSeed,
    store::{KeyValueStore, Store},
};
use std::str::FromStr;

const AUTHOR_SEED: &str = "AUTHOR_SEED";
const ANN_LINK: &str = "ANN_LINK";
const PSK_SEED: &str = "PSK";
const KEYLOAD_LINK: &str = "KEYLOAD_LINK";

#[async_trait]
pub trait SinglePointRepo {
    async fn save_author_seed(seed: &String) -> Result<()>;
    async fn try_load_author_seed() -> Result<String>;
    async fn save_announcement_link(link: &TangleAddress) -> Result<()>;
    async fn save_keyload_link(link: &TangleAddress) -> Result<()>;
    async fn try_load_announcement_link() -> Result<TangleAddress>;
    async fn try_load_psk_seed() -> Result<PskSeed>;
    async fn save_psk_seed(psk: &PskSeed) -> Result<()>;
}

pub struct LocalSinglePointRepo {}

#[async_trait]
impl SinglePointRepo for LocalSinglePointRepo {
    async fn save_author_seed(seed: &String) -> Result<()> {
        if let Ok(_) = KeyValueStore::store(AUTHOR_SEED.into(), seed) {
        } else {
            eprintln!("Storing author seed failed");
        };
        Ok(())
    }
    async fn try_load_author_seed() -> Result<String> {
        KeyValueStore::read(AUTHOR_SEED.into())
    }
    async fn save_announcement_link(link: &TangleAddress) -> Result<()> {
        if let Ok(_) = KeyValueStore::store(ANN_LINK.into(), link) {
        } else {
            eprintln!("Storing announcement link failed");
        };
        Ok(())
    }
    // pub fn try_load_keyload_link() -> Result<TangleAddress> {
    //     match FileStore::read(KEYLOAD_LINK.into()) {
    //         Ok(link_string) => TangleAddress::from_str(link_string.as_str()),
    //         Err(err) => Err(err),
    //     }
    // }
    async fn save_keyload_link(link: &TangleAddress) -> Result<()> {
        if let Ok(_) = KeyValueStore::store(KEYLOAD_LINK.into(), link) {
        } else {
            eprintln!("Storing announcement link failed");
        };
        Ok(())
    }
    async fn try_load_announcement_link() -> Result<TangleAddress> {
        match KeyValueStore::read(ANN_LINK.into()) {
            Ok(ann_link_string) => TangleAddress::from_str(ann_link_string.as_str()),
            Err(err) => Err(err),
        }
    }
    async fn try_load_psk_seed() -> Result<PskSeed> {
        match KeyValueStore::read(PSK_SEED.into()) {
            Ok(seed_string) => {
                let seed_bytes_vec = base64::decode(seed_string)?;
                PskSeed::try_from(seed_bytes_vec)
            }
            Err(err) => Err(err),
        }
    }
    async fn save_psk_seed(psk: &PskSeed) -> Result<()> {
        if let Ok(_) = KeyValueStore::store(PSK_SEED.into(), psk) {
        } else {
            eprintln!("Storing psk seed failed");
        };
        Ok(())
    }
}
