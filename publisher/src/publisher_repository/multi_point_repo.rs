// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::{anyhow, Result};
use async_trait::async_trait;
use iota_streams::app::transport::tangle::TangleAddress;
use shared::{
    model::Point,
    psk_seed::PskSeed,
    store::{AuthorSeedStore, Store},
    // utils,
};
use std::{collections::HashMap, str::FromStr};

#[async_trait]
pub trait MultiPointRepo {
    async fn get_point(point_id: &String, server: &String) -> Result<Point>;

    async fn create_point(point: &Point, server: &String) -> Result<()>;

    async fn save_author_seed(point_id: &String, seed: &String) -> Result<()>;

    async fn try_load_author_seed(point_id: &String) -> Result<String>;

    async fn load_all_author_seeds() -> Result<HashMap<String, String>>;

    async fn save_announcement_link(
        point_id: &String,
        link: &TangleAddress,
        server: &String,
    ) -> Result<()>;

    async fn save_keyload_link(
        point_id: &String,
        link: &TangleAddress,
        server: &String,
    ) -> Result<()>;

    async fn try_load_announcement_link(
        point_id: &String,
        server: &String,
    ) -> Result<TangleAddress>;

    async fn try_load_psk_seed(point_id: &String, server: &String) -> Result<PskSeed>;

    async fn save_psk_seed(point_id: &String, psk: &PskSeed, server: &String) -> Result<()>;
}

pub struct RemoteMultiPointRepo {}

#[async_trait]
impl MultiPointRepo for RemoteMultiPointRepo {
    async fn get_point(point_id: &String, server: &String) -> Result<Point> {
        // let base_url = utils::env::get_unwrapped_server_url();
        let url = format!("{}/point/{}", server, point_id);
        let point = reqwest::get(url).await?.json::<Point>().await?;
        Ok(point)
    }

    async fn create_point(point: &Point, server: &String) -> Result<()> {
        let point_json = serde_json::to_string(point)?;
        // let base_url = utils::env::get_unwrapped_server_url();
        let url = format!("{}/point", server);
        reqwest::Client::new()
            .post(url)
            .header("Content-Type", "application/json")
            .body(point_json)
            .send()
            .await?;
        Ok(())
    }

    async fn save_author_seed(point_id: &String, seed: &String) -> Result<()> {
        AuthorSeedStore::store(point_id.clone(), seed)
    }

    async fn try_load_author_seed(point_id: &String) -> Result<String> {
        AuthorSeedStore::read(point_id.clone())
    }

    /// Reads key-value-pairs of point ids and author seeds as a HashMap
    async fn load_all_author_seeds() -> Result<HashMap<String, String>> {
        AuthorSeedStore::read_all_seeds()
    }

    async fn save_announcement_link(
        point_id: &String,
        link: &TangleAddress,
        server: &String,
    ) -> Result<()> {
        // let base_url = utils::env::get_unwrapped_server_url();
        let url = format!("{}/point/{}", server, point_id);
        // Loading point
        let mut point = Self::get_point(point_id, server).await?;
        // Changing it's announcement link
        point.announcement_link = Some(link.to_string());
        let point_json = serde_json::to_string(&point)?;
        // Saving point
        reqwest::Client::new()
            .put(url)
            .header("Content-Type", "application/json")
            .body(point_json)
            .send()
            .await?;

        Ok(())
    }

    // pub fn try_load_keyload_link() -> Result<TangleAddress> {}

    async fn save_keyload_link(
        point_id: &String,
        link: &TangleAddress,
        server: &String,
    ) -> Result<()> {
        // let base_url = utils::env::get_unwrapped_server_url();
        let url = format!("{}/point/{}", server, point_id);
        // Loading point
        let mut point = Self::get_point(point_id, server).await?;
        // Changing it's keyload link
        point.keyload_link = Some(link.to_string());
        let point_json = serde_json::to_string(&point)?;
        // Saving point
        reqwest::Client::new()
            .put(url)
            .header("Content-Type", "application/json")
            .body(point_json)
            .send()
            .await?;

        Ok(())
    }

    async fn try_load_announcement_link(
        point_id: &String,
        server: &String,
    ) -> Result<TangleAddress> {
        let point = Self::get_point(point_id, server).await?;
        let ann_link = if let Some(link) = point.announcement_link {
            link
        } else {
            return Err(anyhow!("point.announcement_link was None"));
        };
        let ann_link = TangleAddress::from_str(ann_link.as_str())?;

        Ok(ann_link)
    }

    async fn try_load_psk_seed(point_id: &String, server: &String) -> Result<PskSeed> {
        let point = Self::get_point(point_id, server).await?;
        let psk_seed = if let Some(seed) = point.pre_shared_key_seed {
            seed
        } else {
            return Err(anyhow!("point.pre_shared_key_seed was None"));
        };
        let seed_bytes = base64::decode(&psk_seed)?;
        let psk_seed = PskSeed::try_from(seed_bytes)?;

        return Ok(psk_seed);
    }

    async fn save_psk_seed(point_id: &String, psk_seed: &PskSeed, server: &String) -> Result<()> {
        // let base_url = utils::env::get_unwrapped_server_url();
        let url = format!("{}/point/{}", server, point_id);
        // Loading point
        let mut point = Self::get_point(point_id, server).await?;
        // Changing it's keyload link
        point.pre_shared_key_seed = Some(psk_seed.to_string());
        let point_json = serde_json::to_string(&point)?;
        // Saving point
        reqwest::Client::new()
            .put(url)
            .header("Content-Type", "application/json")
            .body(point_json)
            .send()
            .await?;

        Ok(())
    }
}
