// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::Result;
use shared::test_data::TestData;

use crate::as_payload::AsPayload;
use crate::point_author::PointAuthor;
use crate::repo::multi_point_repo::{MultiPointRepo, RemoteMultiPointRepo};
use crate::utils;

pub async fn single_point(
    point_id: String,
    runs: usize,
    node_url: String,
    server: String,
) -> Result<()> {
    let author_seed = RemoteMultiPointRepo::try_load_author_seed(&point_id).await?;
    let mut point_author = if let Ok((last_msg_link, author)) =
        utils::recover_author_of_point(&point_id, &node_url, &server).await
    {
        shared::utils::print_success(); // Shows if recovery was successful
        PointAuthor {
            point_id,
            author,
            last_msg_link,
        }
    } else {
        shared::utils::print_fail(); // Shows if recovery failed

        // Using unwrap() here, because program can't start without an author
        let (last_msg_link, author) = match utils::create_and_remotely_save_author(
            &point_id,
            &author_seed,
            &node_url,
            &server,
        )
        .await
        {
            Ok(created_author) => {
                shared::utils::print_success(); // Shows if creation was successful
                created_author
            }
            Err(err) => {
                shared::utils::print_fail(); // Shows if creation failed
                return Err(err);
            }
        };

        PointAuthor {
            point_id,
            author,
            last_msg_link,
        }
    };

    // Catching up with all pending messages
    point_author.author.sync_state().await?;

    match send_test_data(vec![point_author], runs).await {
        Ok(_) => println!("Test runs completed ✓"),
        Err(err) => {
            println!("Test runs failed ⨯");
            eprintln!("{}", err);
        }
    }
    Ok(())
}

// pub async fn run_test(node_url: &String, server: &String) -> Result<()> {
//     println!("run_test()");
//     let point_count = if let Ok(number) = std::env::var("POINT_COUNT") {
//         if let Ok(count) = u32::from_str_radix(number.as_str(), 10) {
//             count
//         } else {
//             panic!("POINT_COUNT should be an u32");
//         }
//     } else {
//         panic!("POINT_COUNT was not set. Please set POINT_COUNT to an u32 integer");
//     };

//     // HashMap<PointId, AuthorSeed>
//     let author_seeds: HashMap<String, String> = (0..point_count)
//         .map(|num| (format!("T{}", num), "".into()))
//         .collect();
//     let mut point_authors: Vec<PointAuthor> = vec![];

//     for (point_id, author_seed) in author_seeds.into_iter() {
//         let author = if let Ok((last_msg_link, author)) =
//             utils::recover_author_of_point(&point_id, &node_url, server).await
//         {
//             shared::utils::print_success(); // Shows if recovery was successful
//             PointAuthor {
//                 point_id,
//                 author,
//                 last_msg_link,
//             }
//         } else {
//             shared::utils::print_fail(); // Shows if recovery failed

//             // Using unwrap() here, because program can't start without an author
//             let (last_msg_link, author) = match utils::create_and_remotely_save_author(
//                 &point_id,
//                 &author_seed,
//                 &node_url,
//                 server,
//             )
//             .await
//             {
//                 Ok(created_author) => {
//                     shared::utils::print_success(); // Shows if creation was successful
//                     created_author
//                 }
//                 Err(err) => {
//                     shared::utils::print_fail(); // Shows if creation failed
//                     return Err(err);
//                 }
//             };

//             PointAuthor {
//                 point_id,
//                 author,
//                 last_msg_link,
//             }
//         };
//         point_authors.push(author);
//     }

//     // Catching up with all pending messages
//     for point_author in point_authors.iter_mut() {
//         point_author.author.sync_state().await?;
//     }

//     match send_test_data(point_authors).await {
//         Ok(_) => println!("Test runs completed ✓"),
//         Err(err) => {
//             println!("Test runs failed ⨯");
//             eprintln!("{}", err);
//         }
//     }
//     Ok(())
// }

async fn send_test_data(mut point_authors: Vec<PointAuthor>, runs: usize) -> Result<()> {
    println!("Sending {} packages of test data ...\n", runs);
    for run_count in 0..runs {
        for point_author in point_authors.iter_mut() {
            point_author.author.sync_state().await?;

            let test_data = TestData::new(&point_author.point_id, runs);
            let test_data_json = serde_json::to_string(&test_data).unwrap();

            println!("Run {}: {:?}", run_count, test_data);
            let (msg_link, _) = point_author
                .author
                .send_signed_packet(
                    &point_author.last_msg_link,
                    &"".as_payload(), // We don't want to send public payloads
                    &test_data_json.as_str().as_payload(),
                )
                .await
                .unwrap();

            point_author.last_msg_link = msg_link;

            println!(); // Empty line for readability
        }
    }

    for point_author in point_authors.iter_mut() {
        println!("END {:?}", point_author.point_id);
        let (msg_link, _) = point_author
            .author
            .send_signed_packet(
                &point_author.last_msg_link,
                &"".as_payload(), // We don't want to send public payloads
                &"END".as_payload(),
            )
            .await
            .unwrap();
        point_author.last_msg_link = msg_link;
        println!(); // Empty line for readability
    }
    Ok(())
}
