// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::Result;

use crate::as_payload::AsPayload;
use crate::point_author::PointAuthor;

pub async fn multiple(mut point_authors: Vec<PointAuthor>) -> Result<()> {
    for point_author in point_authors.iter_mut() {
        println!("END {:?}", point_author.point_id);
        let (msg_link, _) = point_author
            .author
            .send_signed_packet(
                &point_author.last_msg_link,
                &"".as_payload(), // We don't want to send public payloads
                &"END".as_payload(),
            )
            .await
            .unwrap();
        point_author.last_msg_link = msg_link;
        println!(); // Empty line for readability
    }
    Ok(())
}
