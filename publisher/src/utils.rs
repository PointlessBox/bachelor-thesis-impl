// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::time::Duration;

use anyhow::{Error, Result};
use chrono::Utc;
use iota_streams::app::transport::tangle::client::Client;
use iota_streams::app_channels::api::tangle::{Author, ChannelType};
use iota_streams::app_channels::api::{psk_from_seed, pskid_from_psk, Psk, PskId};
use rand::Rng;
use shared::model::Point;
use shared::psk_seed::PskSeed;
use shared::timed_measurement::TimedMeasurement;

use crate::point_author::PointAuthor;
use crate::repo;
use crate::repo::multi_point_repo::{MultiPointRepo, RemoteMultiPointRepo};
use crate::repo::single_point_repo::{LocalSinglePointRepo, SinglePointRepo};
use crate::PrevMsgLink;

/// Tries to recover a previously saved author with the last message link that was used.
/// # Returns
/// A tuple with `(last_msg_link, author)`
pub async fn try_recover_author(node_url: &String) -> Result<(PrevMsgLink, Author<Client>)> {
    let client = Client::new_from_url(node_url.as_str());
    let error = Err(Error::new(std::fmt::Error));

    print!("Trying to load store ... ");

    // Try to load a saved author seed.
    // If loading fails then generate a new one and save it
    let author_seed = if let Ok(seed) = LocalSinglePointRepo::try_load_author_seed().await {
        seed
    } else {
        return error;
    };

    // Try to load the last announcement link.
    // If it fails, then create a new one and save it
    let ann_link = if let Ok(link) = LocalSinglePointRepo::try_load_announcement_link().await {
        link
    } else {
        return error;
    };

    let mut author = if let Ok(author) =
        repo::recover_author(&author_seed, &ann_link, ChannelType::SingleBranch, client).await
    {
        author
    } else {
        return error;
    };

    author.sync_state().await?;

    let mut last_msg = ann_link;
    for (_pk, cursor) in author.fetch_state()?.iter() {
        last_msg = cursor.link;
    }

    // let last_msg = if let Ok(msgs) = author.fetch_next_msgs().await {
    //     if let Some(msg) = msgs.last() {
    //         msg.link
    //     } else {
    //         return error;
    //     }
    // } else {
    //     return error;
    // };

    Ok((last_msg, author))
}

/// Creates a new author and announces a new channel.
/// # Returns
/// A tuple with `(ann_link, author)`.
pub async fn create_and_locally_save_author(
    node_url: &String,
) -> Result<(PrevMsgLink, Author<Client>)> {
    let client = Client::new_from_url(node_url.as_str());

    print!("Creating and storing new author for point ... ");

    // Try to load a saved author seed.
    // If loading fails then generate a new one and save it
    let author_seed = {
        let seed = shared::utils::seed::generate_with_32_bytes();
        LocalSinglePointRepo::save_author_seed(&seed).await?;
        seed
    };

    let mut author = Author::new(&author_seed, ChannelType::SingleBranch, client);

    // Try to load the last announcement link.
    // If it fails, then create a new one and save it
    let ann_link = {
        let link = author.send_announce().await?;
        LocalSinglePointRepo::save_announcement_link(&link).await?;
        link
    };

    let (psk, psk_id) = {
        let psk_seed = PskSeed::try_from(rand::thread_rng().gen::<[u8; 32]>().to_vec())?;
        let (psk, psk_id) = generate_psk(&psk_seed);
        LocalSinglePointRepo::save_psk_seed(&psk_seed).await?;
        (psk, psk_id)
    };
    author.store_psk(psk_id, psk)?;

    let (keyload_link, _) = author.send_keyload_for_everyone(&ann_link).await?;
    LocalSinglePointRepo::save_keyload_link(&keyload_link).await?;

    Ok((keyload_link, author))
}

/// Tries to recover the previously saved author of the given point id with the last message link that was used.
/// # Returns
/// A tuple with `(last_msg_link, author)`
pub async fn recover_author_of_point(
    point_id: &String,
    node_url: &String,
    server: &String,
) -> Result<(PrevMsgLink, Author<Client>)> {
    print!("Trying to load author of point  {} ... ", point_id);
    let client = Client::new_from_url(node_url.as_str());
    let error = Err(Error::new(std::fmt::Error));

    // Try to load a saved author seed.
    // If loading fails then generate a new one and save it
    let author_seed = if let Ok(seed) = RemoteMultiPointRepo::try_load_author_seed(point_id).await {
        // Check if loaded seed is empty
        if seed.len() != 0 {
            seed
        } else {
            return error;
        }
    } else {
        return error;
    };

    // Try to load the last announcement link.
    // If it fails, then create a new one and save it
    let ann_link = if let Ok(link) =
        RemoteMultiPointRepo::try_load_announcement_link(point_id, server).await
    {
        link
    } else {
        return error;
    };

    let mut author = if let Ok(author) =
        repo::recover_author(&author_seed, &ann_link, ChannelType::SingleBranch, client).await
    {
        author
    } else {
        return error;
    };

    author.sync_state().await?;

    let mut last_msg = ann_link;
    for (_pk, cursor) in author.fetch_state()?.iter() {
        last_msg = cursor.link;
    }

    Ok((last_msg, author))
}

pub async fn recover_all_authors(node_url: &String, server: &String) -> Result<Vec<PointAuthor>> {
    let author_seeds = RemoteMultiPointRepo::load_all_author_seeds().await?;
    let mut point_authors: Vec<PointAuthor> = vec![];

    for (point_id, _author_seed) in author_seeds.into_iter() {
        let author = if let Ok((last_msg_link, author)) =
            recover_author_of_point(&point_id, &node_url, server).await
        {
            shared::utils::print_success(); // Shows if recovery was successful
            PointAuthor {
                point_id,
                author,
                last_msg_link,
            }
        } else {
            return Err(anyhow::anyhow!("Could not load point authors"));
        };
        point_authors.push(author);
    }
    return Ok(point_authors);
}

/// Creates a new author and announces a new channel.
/// # Returns
/// A tuple with `(ann_link, author)`.
pub async fn create_and_remotely_save_author(
    point_id: &String,
    author_seed: &String,
    node_url: &String,
    server: &String,
) -> Result<(PrevMsgLink, Author<Client>)> {
    print!(
        "Creating and storing new author for point {} ... ",
        point_id
    );
    let client = Client::new_from_url(node_url.as_str());

    let mut author_seed = author_seed.clone();

    // If the author_seed is empty, then no author was created and saved.
    if author_seed.len() == 0 {
        print!("Creating point {} ... ", point_id);
        // Create new point in backend
        let point = Point {
            measure_point_id: point_id.clone(),
            announcement_link: None,
            keyload_link: None,
            pre_shared_key_seed: None,
        };
        RemoteMultiPointRepo::create_point(&point, server).await?;

        author_seed = {
            let seed = shared::utils::seed::generate_with_32_bytes();
            RemoteMultiPointRepo::save_author_seed(point_id, &seed).await?;
            seed
        };
    }

    let mut author = Author::new(&author_seed, ChannelType::SingleBranch, client);

    // Try to load the last announcement link.
    // If it fails, then create a new one and save it
    let ann_link = {
        let link = author.send_announce().await?;
        RemoteMultiPointRepo::save_announcement_link(point_id, &link, server).await?;
        link
    };

    let (psk, psk_id) = {
        let psk_seed = PskSeed::try_from(rand::thread_rng().gen::<[u8; 32]>().to_vec())?;
        let (psk, psk_id) = generate_psk(&psk_seed);
        RemoteMultiPointRepo::save_psk_seed(point_id, &psk_seed, server).await?;
        (psk, psk_id)
    };
    author.store_psk(psk_id, psk)?;

    let (keyload_link, _) = author.send_keyload_for_everyone(&ann_link).await?;
    RemoteMultiPointRepo::save_keyload_link(point_id, &keyload_link, server).await?;

    Ok((keyload_link, author))
}

/// Generates a pre-shared-key (PSK) from the given PskSeed
/// # Returns
/// A tuple with `(psk, psk_id)`
pub fn generate_psk(seed: &PskSeed) -> (Psk, PskId) {
    // let key_seed = rand::thread_rng().gen::<[u8; 32]>();
    let key_seed = seed.as_bytes();
    // Generate a pre-shared-key from that seed
    let psk = psk_from_seed(key_seed);
    // Get the id of the psk
    let psk_id = pskid_from_psk(&psk);

    // Return both as a tuple
    return (psk, psk_id);
}

/// Creates a new Duration from the given amount of seconds. This is just for readability :)
/// # Returns
/// `Duration::new(s, 0)` where `s` is the parameter `s` that was given.
pub fn sec(s: u64) -> Duration {
    Duration::from_secs(s)
}

/// Generates a TimedMeasurement for the given measure_point_id.
/// Contains chrono::Utc::now() as 'timestamp' and a random f64 as 'measurement'.
/// # Returns
/// ```
/// TimedMeasurement {
///     timestamp: Utc::now().to_string(),
///     measure_point_id,
///     measurement: rand::thread_rng().gen::<f64>(),
/// }
/// ```
pub fn generate_timed_measurement(measure_point_id: String) -> TimedMeasurement {
    TimedMeasurement {
        timestamp: Utc::now().to_rfc3339(),
        measure_point_id,
        measurement: rand::thread_rng().gen::<f64>(),
    }
}
