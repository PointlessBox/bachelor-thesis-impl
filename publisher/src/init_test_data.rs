// Bachelor Thesis Impl
// Copyright (C) 2022  Fabian Kraft

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use anyhow::Result;
use iota_streams::app::transport::tangle::client::Client;
use iota_streams::app_channels::api::tangle::{Author, ChannelType};
use rand::Rng;
use shared::model::Point;
use shared::psk_seed::PskSeed;

use crate::repo::multi_point_repo::{MultiPointRepo, RemoteMultiPointRepo};
use crate::PrevMsgLink;

pub async fn generate_init_data_for(
    point_id: &String,
    node_url: &String,
    server: &String,
) -> Result<()> {
    match create_and_remotely_save_author(point_id, node_url, server).await {
        Ok(created_author) => {
            shared::utils::print_success(); // Shows if creation was successful
            created_author
        }
        Err(err) => {
            shared::utils::print_fail(); // Shows if creation failed
            return Err(err);
        }
    };

    Ok(())
}

/// Creates a new author and announces a new channel.
/// # Returns
/// A tuple with `(ann_link, author)`.
pub async fn create_and_remotely_save_author(
    point_id: &String,
    node_url: &String,
    server: &String,
) -> Result<(PrevMsgLink, Author<Client>)> {
    print!(
        "Creating and storing new author for point {} ... ",
        point_id
    );
    let client = Client::new_from_url(node_url.as_str());

    // If the author_seed is empty, then no author was created and saved.
    println!("Creating point {} ... ", point_id);
    // Create new point in backend
    let point = Point {
        measure_point_id: point_id.clone(),
        announcement_link: None,
        keyload_link: None,
        pre_shared_key_seed: None,
    };
    RemoteMultiPointRepo::create_point(&point, server).await?;
    println!("Point {} created", point_id);

    let author_seed = {
        let seed = shared::utils::seed::generate_with_32_bytes();
        RemoteMultiPointRepo::save_author_seed(point_id, &seed).await?;
        seed
    };
    println!("Point {} saved", point_id);

    let mut author = Author::new(&author_seed, ChannelType::SingleBranch, client);
    println!("Author for {} created", point_id);

    // Try to load the last announcement link.
    // If it fails, then create a new one and save it
    let ann_link = {
        let link = author.send_announce().await?;
        RemoteMultiPointRepo::save_announcement_link(point_id, &link, server).await?;
        link
    };
    println!("Announcement for {} created", point_id);

    let (psk, psk_id) = {
        let psk_seed = PskSeed::try_from(rand::thread_rng().gen::<[u8; 32]>().to_vec())?;
        let (psk, psk_id) = crate::utils::generate_psk(&psk_seed);
        RemoteMultiPointRepo::save_psk_seed(point_id, &psk_seed, server).await?;
        (psk, psk_id)
    };
    author.store_psk(psk_id, psk)?;

    let (keyload_link, _) = author.send_keyload_for_everyone(&ann_link).await?;
    RemoteMultiPointRepo::save_keyload_link(point_id, &keyload_link, server).await?;
    println!("Keyload for {} sent", point_id);

    Ok((keyload_link, author))
}

// pub async fn init_test_data(point_count: usize, node_url: &String) -> Result<()> {
//     let point_ids = (0..point_count)
//         .map(|i| format!("MP-{i}"))
//         .collect::<Vec<String>>();
//     for point_id in point_ids {
//         match create_and_remotely_save_author(&point_id, &node_url).await {
//             Ok(created_author) => {
//                 shared::utils::print_success(); // Shows if creation was successful
//                 created_author
//             }
//             Err(err) => {
//                 shared::utils::print_fail(); // Shows if creation failed
//                 return Err(err);
//             }
//         };
//     }

//     Ok(())
// }

// /// Creates a new author and announces a new channel.
// /// # Returns
// /// A tuple with `(ann_link, author)`.
// pub async fn create_and_remotely_save_author(
//     point_id: &String,
//     node_url: &String,
// ) -> Result<(PrevMsgLink, Author<Client>)> {
//     print!(
//         "Creating and storing new author for point {} ... ",
//         point_id
//     );
//     let client = Client::new_from_url(node_url.as_str());

//     // If the author_seed is empty, then no author was created and saved.
//     print!("Creating point {} ... ", point_id);
//     // Create new point in backend
//     let point = Point {
//         measure_point_id: point_id.clone(),
//         announcement_link: None,
//         keyload_link: None,
//         pre_shared_key_seed: None,
//     };
//     RemoteMultiPointRepo::create_point(&point).await?;

//     let author_seed = {
//         let seed = shared::utils::seed::generate_with_32_bytes();
//         RemoteMultiPointRepo::save_author_seed(point_id, &seed).await?;
//         seed
//     };

//     let mut author = Author::new(&author_seed, ChannelType::SingleBranch, client);

//     // Try to load the last announcement link.
//     // If it fails, then create a new one and save it
//     let ann_link = {
//         let link = author.send_announce().await?;
//         RemoteMultiPointRepo::save_announcement_link(point_id, &link).await?;
//         link
//     };

//     let (psk, psk_id) = {
//         let psk_seed = PskSeed::try_from(rand::thread_rng().gen::<[u8; 32]>().to_vec())?;
//         let (psk, psk_id) = crate::utils::generate_psk(&psk_seed);
//         RemoteMultiPointRepo::save_psk_seed(point_id, &psk_seed).await?;
//         (psk, psk_id)
//     };
//     author.store_psk(psk_id, psk)?;

//     let (keyload_link, _) = author.send_keyload_for_everyone(&ann_link).await?;
//     RemoteMultiPointRepo::save_keyload_link(point_id, &keyload_link).await?;

//     Ok((keyload_link, author))
// }
